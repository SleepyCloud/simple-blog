from django.db import models
from datetime import datetime

# Create your models here.
class Post(models.Model):
    def __str__(self):              
        return self.title
    title = models.CharField(max_length=50, default='')
    post_content = models.CharField(max_length=1500)
    published = models.DateTimeField('date published', default=datetime.now)

class Comment(models.Model):
    def __str__(self):              
        return self.comment_name

    post = models.ForeignKey(Post)
    comment_name = models.CharField(max_length=30,default='Anonymous' )
    comment_body = models.CharField(max_length=200)
    published = models.DateTimeField('date published', default=datetime.now)

