from django.shortcuts import render, get_object_or_404
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from models import Post, Comment

# Create your views here.
def index(request):
    posts = Post.objects.all()
    template = loader.get_template('blog/index.html')
    context = RequestContext(request, {'posts' : posts })
    return HttpResponse(template.render(context))

def blog_post(request, post_id):
    blog_post = get_object_or_404(Post, pk=post_id)

    template = loader.get_template('blog/post.html')
    context = RequestContext(request, {
        'blog_post': blog_post,
        })
    return HttpResponse(template.render(context))

def comment(request, post_id):
    blog_post = Post.objects.get(pk=post_id)
    name=request.POST.get('name')
    body=request.POST.get('body')
    blog_post.comment_set.create(comment_name=name, comment_body=body)

    return HttpResponseRedirect('/blog/'+str(post_id)+'/')
