from django.conf.urls import patterns, url

from blog import views

urlpatterns = patterns('',
        url(r'^(?P<post_id>\d+)/$', views.blog_post, name='blog_post'),
        url(r'^(?P<post_id>\d+)/comment/$', views.comment, name='comment'),
        )
